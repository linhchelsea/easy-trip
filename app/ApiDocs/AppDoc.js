/**
 * @api {GET} http://localhost:3333/api/v1/version/:platform/update?version=:version Check for update App
 * @apiName Check for update App
 * @apiGroup App
 *
 * @apiParam {String} platform Platform
 * @apiParam {String} version Current version
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 *{
 *    "status": 200,
 *    "data": {
 *        "status": "none|force_update|optional"
 *    },
 *    "message": "success",
 *    "error": 0
 *}
 */

/**
 * @api {GET} http://localhost:3333/api/v1/environment/:platform?version=:version Get environment variables
 * @apiName Get environment variables
 * @apiGroup App
 *
 * @apiParam {String} platform Platform
 * @apiParam {String} version Current version
 *
 * @apiSuccess {Number} [error=0] error code
 * @apiSuccess {String} [message='success'] response message
 * @apiSuccess {Object[]} data data response
 * @apiSuccess {Number} [status=200] status response
 * @apiSuccessExample Success response:
 *{
 *    "status": 200,
 *    "data": {
 *        "enc": "123",
 *        "data": "abc"
 *    },
 *    "message": "success",
 *    "error": 0
 *}
 */
