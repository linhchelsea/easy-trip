'use strict';

const Schema = use('Schema');

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.bigIncrements();
      table.string('email', 254).notNullable().unique();
      table.string('username').notNullable();
      table.string('password', 60).notNullable();
      table.timestamps();
    });
  }

  down () {
    this.dropIfExists('users');
  }
}

module.exports = UserSchema;
